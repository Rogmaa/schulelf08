/**
 * file        : 20240627�0741 (20240617�1741, base 20220809�1741)
 * summary     : Introduce constructors and destructors
 */

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Scanner;

class Main
{
   private static final ArrayList<Animal> animals = new ArrayList<>();

   public static void AutomaticStoryRun() {
      System.out.println("Automatic story board run ...");

      // Fill zoo
      animals.add(new Croco("Schnappi"));
      animals.add(new Croco("GreenBeast"));
      animals.add(new Rhino());
      animals.get(animals.size() - 1).setColor("Turquoise");   // Access the last element of the list
      animals.get(animals.size() - 1).setName("Terminator");   // Access the last element of the list
      animals.add(new Rhino());

      System.out.println("Polymorphes Fr�hst�ck:");
      for (Animal a : animals) {
         a.feed();
         System.out.println(" - Animal " + a.getName() + " " + a.getColor() + " size = " + a.getSize());
      }
      System.out.println("Polymorphes Abendessen:");
      for (Animal a : animals) {
         a.feed();
         System.out.println(" - Animal " + a.getName() + " " + a.getColor() + " size = " + a.getSize());
      }
   }

   public static void CreateAnimalRandom() {
      System.out.println("Create some random animal ...");
      Animal a = Animal.GetRandomAnimal();                             // Factory-Methode
      animals.add(a);
   }

   public static void CreateCroco() {
      System.out.println("Create new Croco ...");
      Animal c = new Croco();
      animals.add(c);
   }

   public static void CreateRhino() {
      System.out.println("Create new Rhino ...");
      animals.add(new Rhino());
   }

   public static void FeedAll() {
      System.out.println("Feed all ...");
      for (Animal a : animals) {
         a.feed();
      }
   }

   public static void ListAllInfo() {
      System.out.println("List full info:");
      for (Animal a : animals) {
         //System.out.println(" - Animal Nr." + a.getNummer() + " " + a.getName() + " " + a.getColor() + " size = " + a.getSize());
         String s = MessageFormat.format (" - Animal Nr. {0} {1} {2} size = {3}"
                                          , String.format("%-4s", a.getNummer())
                                           , String.format("%-12s", a.getName())
                                            , String.format("%-12s", a.getColor())
                                             , a.getSize()
                                             );
         System.out.println(s);
      }
   }

   public static void main(String[] args)
   {
      System.out.println("*** Animals Five ***");

       try (Scanner scn = new Scanner(System.in)) {
           boolean bRun = true;
           while (bRun)
           {
               System.out.println("--------------------------------------------------------------------------------------------");
               System.out.println("Menu: a = Animal, c = Croco, r = Rhino, f = Feed, i = Info, s = Story, z = Zaehler, x = Exit");
               String sKey = scn.nextLine();
               
               switch (sKey)
               {
                   case "a" -> CreateAnimalRandom();
                   case "c" -> CreateCroco();
                   case "f" -> FeedAll();
                   case "i" -> ListAllInfo();
                   case "r" -> CreateRhino();
                   case "s" -> AutomaticStoryRun();
                   case "x" -> bRun = false;
                   case "z" -> System.out.println("Es sind " + Animal.animalCounter + " Tiere im Spiel.");
                   default -> System.out.println("Invalid selection: \"" + sKey + "\"");
               }
           }}
      System.out.println("*** Goodbye ***");
   }
}
