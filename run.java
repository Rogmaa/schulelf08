/**
 *  file : 2022-08-18`11:00 (after 2022-08-16`16:00)
 *  summary :
 *  Hinweis: Um import java.sql.ResultSet; sichbar zu machen, fügen Sie
 *            in module-info.java die Zeile "requires java.sql;" ein.
 */

package terra2;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class run {

    private static void AbfrageOrginal()
    {
        MySQLConnection mySqlConnection = null;        // Declaration
        try {
            mySqlConnection = new MySQLConnection();   // Initialisierung
            mySqlConnection.create();
            String sqlStatement = "SELECT O.Name, O.Einwohner, L.Name AS L_Name " +
            "FROM Ort O INNER JOIN Land L ON O.LNR = L.LNR LIMIT 10";
            System.out.println("Ortsname, Einwohner, L_Name");
            System.out.println("----------------------------------------------");
            ResultSet rs = mySqlConnection.getData(sqlStatement);

            while (rs.next())
            {
                String ortsName = rs.getString("Name");
                int anzahlEinwohner = rs.getInt("Einwohner");
                String landName = rs.getString("L_Name");

                System.out.format("%s,\t %s,\t %s\n", ortsName, anzahlEinwohner, landName);
            }
        } catch (SQLException e) {
        } finally {
            if (mySqlConnection != null)
            {
                //mySqlConnection.close();
            }
        }
    }

    private static void AbfragMitAuswahl()
    {
        System.out.print("Geben Sie den oder die Anfangsbuchstaben der gesuchten Staedte ein (max. 4 Zeichen) >");
        try (Scanner scn = new Scanner(System.in)) {
            String sInput = scn.nextLine();

            // SQL-Injektions-Gefahr
            // Der korrekte Umgang mit Anwender-Eingaben wäre die Verwendung von 'Predefined Queries'.
            // Minimal-Schutz: Erlaube nur die ersten vier Zeichen, mit nur vier Buchstaben können
            //      die User keinen Schaden anrichten (oder doch?).
            if (sInput.length() > 4)
            {
                sInput = sInput.substring(0,4); // Minimal-Schutz, erlaubt nur die ersten vier Zeichen
            }

            MySQLConnection mySqlConnection = null;        // Deklaration
            try {
                mySqlConnection = new MySQLConnection();       // Initialisierung
                mySqlConnection.create();
                String sqlStatement = "SELECT O.Name, O.Einwohner, L.Name AS L_Name " +
                 "FROM Ort O INNER JOIN Land L ON O.LNR = L.LNR WHERE O.Name LIKE '" + sInput + "%'"; // Achtung SQL-Injection
                System.out.println("Ortsname, Einwohner, L_Name");
                System.out.println("----------------------------------------------");
                ResultSet rs = mySqlConnection.getData(sqlStatement);

                while (rs.next())
                {
                    String ortsName = rs.getString("Name");
                    int anzahlEinwohner = rs.getInt("Einwohner");
                    String landName = rs.getString("L_Name");

                    System.out.format("%-32s, %-16s, %-16s\n", ortsName, anzahlEinwohner, landName);
                }
            } catch (SQLException e) {
            } finally {
                if (mySqlConnection != null)
                {
                    //mySqlConnection.close();
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("*** Hallo Terra2 ***");

        boolean bRun = true;
        while (bRun)
        {
            System.out.println("-------------------------------------------------------------");
            System.out.print("Menu: a = Abfrage mit Auswahl, o = Orginale Abfrage, x = Exit >");
            String sSelection;
            try (Scanner scn = new Scanner(System.in)) {
                sSelection = scn.nextLine();
            }

            switch (sSelection)
            {
                   case "a", "A" -> // Fall Through
                       AbfragMitAuswahl();
                   case "o", "O" -> AbfrageOrginal();
                   case "x", "X" -> bRun = false;
                   default -> System.out.println("Falsche Eingabe : " + sSelection);
            }
            // Fall Through
                    }
        System.out.println("Auf Wiedersehen!");
    }
}
