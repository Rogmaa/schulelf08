/**
 *  file : 2022-08-18`15:21 (after 2022-08-18`11:00, 2022-08-16`16:00)
 *  summary : This demonstrates
 *                • With Keyboard menu and somewhat refactured
 *                • User of prepared statement
 *  reference : E.g. medium.com/swlh/preventing-sql-injection-attack-with-java-prepared-statement-259611281e4d [ref 20220818°1525]
 */

package terra3;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Program {

    private static void AbfrageOrginal()
    {
        MySQLConnection mySqlConnection = null;
        try {
            mySqlConnection = new MySQLConnection();
            mySqlConnection.create();
            String sqlStatement = "SELECT O.Name, O.Einwohner, L.Name AS L_Name " +
            "FROM Ort O INNER JOIN Land L ON O.LNR = L.LNR LIMIT 10";
            System.out.println("Ortsname, Einwohner, L_Name");
            System.out.println("----------------------------------------------");
            ResultSet rs = mySqlConnection.getData(sqlStatement);

            while (rs.next())
            {
                String ortsName = rs.getString("Name");
                int anzahlEinwohner = rs.getInt("Einwohner");
                String landName = rs.getString("L_Name");

                System.out.format("%s,\t %s,\t %s\n", ortsName, anzahlEinwohner, landName);
            }
        } catch (SQLException e) {
        } finally {
            if (mySqlConnection != null)
            {
                //mySqlConnection.close();
            }
        }
    }

    private static void AbfragMitAuswahl()
    {
        System.out.print("Geben Sie den oder die Anfangsbuchstaben der gesuchten Staedte ein (max. 4 Zeichen) >");
        Scanner scn = new Scanner(System.in);
        String sUserInput = scn.nextLine();

        MySQLConnection conn = null;
        try {
            conn = new MySQLConnection();
            conn.create();

            // Question mark is place holder for user input
            String sSqlStmt = "SELECT O.Name, O.Einwohner, L.Name AS L_Name"
                             + " FROM Ort O INNER JOIN Land L ON O.LNR = L.LNR"
                              + " WHERE O.Name LIKE ?;"
                               ;

            PreparedStatement prepstmt = conn.con.prepareStatement(sSqlStmt);
            prepstmt.setString(1, sUserInput + "%"); // It didn't work, when the percent sign was left in the statement string above
            ResultSet rs = prepstmt.executeQuery();

            System.out.println("Ortsname, Einwohner, L_Name");
            System.out.println("----------------------------------------------");
            while (rs.next())
            {
                String ortsName = rs.getString("Name");
                int anzahlEinwohner = rs.getInt("Einwohner");
                String landName = rs.getString("L_Name");

                System.out.format("%-32s, %-16s, %-16s\n", ortsName, anzahlEinwohner, landName);
            }
        } catch (SQLException e) {
        } finally {
            if (conn != null)
            {
                //mySqlConnection.close();
                scn.close();
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("*** Hallo Terra3 ***");

        boolean bRun = true;
        while (bRun)
        {
            System.out.println("-------------------------------------------------------------");
            System.out.print("Menu: a = Abfrage mit Auswahl, o = Orginale Abfrage, x = Exit >");
            try (Scanner scn = new Scanner(System.in)) {
                String sSelection = scn.nextLine();

                switch (sSelection)
                {
                       case "a", "A" -> // Fall Through
                           AbfragMitAuswahl();
                       case "o", "O" -> AbfrageOrginal();
                       case "x", "X" -> bRun = false;
                       default -> System.out.println("Falsche Eingabe : " + sSelection);
                }
                // Fall Through
                            }
        }
        System.out.println("Auf Wiedersehen!");
    }
}
